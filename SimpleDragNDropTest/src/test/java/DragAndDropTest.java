import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by guska on 06.04.16.
 */
public class DragAndDropTest extends DragAndDropTestAbstractTest {

    String path = "file:///home/guska/IdeaProjects/HW_16/drag_and_drop2/index.html";

    @BeforeMethod
    public void beforeMethod()
    {
        dragAndDropPage.openPage(path);
    }

    @Test
    public void dragAndDropToTrashTest() throws InterruptedException {

        List<WebElement> dragElementsList = dragAndDropPage.dragElementsFinder();

        softAssert.assertEquals(dragAndDropPage.dragElement(dragElementsList.get(0), dragAndDropPage.trash)
                .getAletrText(), "Are you sure that you want to delete?", "Alert isn't found");
        Thread.sleep(250);
        softAssert.assertTrue(dragAndDropPage.dismissOrAcceptAlert(true)
                .isElementOnPage(dragElementsList.get(0)), "Page isn't conteins this element");
        softAssert.assertFalse(dragAndDropPage.dragElement(dragElementsList.get(0), dragAndDropPage.trash)
                .dismissOrAcceptAlert(false)
                .isElementOnPage(dragElementsList.get(0)), "Page is conteins this element");
        softAssert.assertAll();

    }

    @Test
    public void dragAndDropSortTest() throws InterruptedException {

      dragAndDropPage.dragElementSort(dragAndDropPage.getDragElementsWithCoord(), false);
      softAssert.assertTrue(dragAndDropPage.isElementSortByAsc(dragAndDropPage.dragElementsFinder()), "Elements aren't sorted by ASC");
      Thread.sleep(5000);
      dragAndDropPage.dragElementSort(dragAndDropPage.getDragElementsWithCoord(), true);
      softAssert.assertFalse(dragAndDropPage.isElementSortByAsc(dragAndDropPage.dragElementsFinder()), "Elements aren't sorted by DESC");
      Thread.sleep(5000);
      softAssert.assertAll();

    }

}
