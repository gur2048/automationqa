import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

/**
 * Created by guska on 28.03.16.
 */
public abstract class DragAndDropTestAbstractTest {
    static WebDriver driver;
    static Logger log;
    static SoftAssert softAssert;
    static DragAndDropPage dragAndDropPage;

    @BeforeTest
    public void beforeTest()
    {
        driver = new FirefoxDriver();
        softAssert = new SoftAssert();
        dragAndDropPage = new DragAndDropPage(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        log = Logger.getLogger(DragAndDropTestAbstractTest.class);
    }

    @AfterTest
    public void afterTest()
    {
        driver.quit();
    }
}
