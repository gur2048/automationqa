import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by guska on 06.04.16.
 */
public class DragAndDropPage {

    public static Logger log = Logger.getLogger(DragAndDropPage.class);
    protected WebDriver driver;
    protected WebDriverWait wait;

    @FindBy(xpath = ".//*[@id='sortable']/li[.='1']")
    private WebElement element1;

    @FindBy(xpath = ".//*[@id='sortable']/li[.='2']")
    private WebElement element2;

    @FindBy(xpath = ".//*[@id='sortable']/li[.='3']")
    private WebElement element3;

    @FindBy(xpath = ".//*[@id='sortable']/li[.='4']")
    private WebElement element4;

    @FindBy(xpath = ".//*[@id='sortable']/li[.='5']")
    private WebElement element5;

    @FindBy(xpath = ".//*[@id='sortable']/li[.='6']")
    private WebElement element6;

    @FindBy(xpath = ".//*[@id='sortable']/li[.='7']")
    private WebElement element7;

    @FindBy(xpath = ".//*[@id='drop']")
    protected WebElement trash;

    @FindBy(xpath = ".//*[.='Drag and Drop']")
    protected WebElement lable;

    String URL = "/src/main/resources/drag_and_drop2/index.html";

    public DragAndDropPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    public DragAndDropPage openPage(String path) {
        driver.get(path);
        return this;
    }

    public List<List<Object>> getDragElementsWithCoord() {

        List<WebElement> dradElements = driver.findElements(By.xpath(".//*[@id='sortable']/li"));
        List<List<Integer>> elementsCoordinates = getElementsCoordinates(dradElements);
        List<List<Object>> DragElementsWithCoord = new ArrayList<List<Object>>();


        for (int i = 0; i < dradElements.size(); i++)
        {
            DragElementsWithCoord.add(i, new ArrayList<Object>(Arrays.asList(dradElements.get(i), elementsCoordinates.get(i))));
        }

        return DragElementsWithCoord;
    }

    public List<WebElement> dragElementsFinder()
    {
        return driver.findElements(By.xpath(".//*[@id='sortable']/li"));
    }


    public void dragElementSort(List<List<Object>> DragElementsWithCoord, boolean byDESC) throws InterruptedException {

        for (int i = DragElementsWithCoord.size() - 1; i > 0; i--)
        {
            for (int j = 0; j < i; j++)
            {
                DragElementsWithCoord = getDragElementsWithCoord();
                int currentWebElementNum = Integer.parseInt(((WebElement) DragElementsWithCoord.get(j).get(0)).getText());
                int nextWebElementNum = Integer.parseInt(((WebElement) DragElementsWithCoord.get(j + 1).get(0)).getText());
                WebElement currentWebElement = ((WebElement) DragElementsWithCoord.get(j).get(0));
                WebElement nextWebElement = ((WebElement) DragElementsWithCoord.get(j + 1).get(0));
                int X_CoordOfСurrentWebElement = ((List<Integer>)DragElementsWithCoord.get(j).get(1)).get(0);
                int y_CoordOfСurrentWebElement = ((List<Integer>)DragElementsWithCoord.get(j).get(1)).get(1)-2;
                if (byDESC)
                {
                    if (currentWebElementNum < nextWebElementNum)
                    {
                        dragElementByCoord(nextWebElement, currentWebElement, X_CoordOfСurrentWebElement, y_CoordOfСurrentWebElement);
                    }
                }
                else
                {
                    if (currentWebElementNum > nextWebElementNum)
                    {
                        dragElementByCoord(nextWebElement, currentWebElement, X_CoordOfСurrentWebElement, y_CoordOfСurrentWebElement);
                    }
                }
            }
        }
    }

    public boolean isElementSortByAsc(List<WebElement> DragElements)
    {
        boolean sortByAsc = false;

        for (int i = 0; i < DragElements.size(); i++ )
        {
            if (Integer.parseInt(DragElements.get(i).getText()) == i + 1)
            {
                sortByAsc = true;
            }
            else
            {
                sortByAsc = false;
            }
        }
        return sortByAsc;
    }


    public List<List<Integer>> getElementsCoordinates (List<WebElement> webElementList)
    {
        List<List<Integer>> ElementsCoordinates = new ArrayList<List<Integer>>();
        for (int i = 0; i < webElementList.size(); i++){
            Point point = webElementList.get(i).getLocation();
            ElementsCoordinates.add(i, new ArrayList<Integer>(Arrays.asList(point.getX(), point.getY())));
        }
        return ElementsCoordinates;
    }


    public DragAndDropPage dragElement(WebElement dragElement, WebElement dropElement) {
        Actions actions = new Actions(driver);
        actions.dragAndDrop(dragElement, dropElement)
                .build()
                .perform();
        return this;
    }

    public DragAndDropPage dragElementByCoord(WebElement dragElement, WebElement dropElement, int xCoor, int yCoor) throws InterruptedException {

        Actions actions = new Actions(driver);
        Thread.sleep(250);
        actions.clickAndHold(dragElement);
        Thread.sleep(250);
        actions.moveToElement(dropElement, xCoor, yCoor);
        Thread.sleep(250);
        actions.perform();
        Thread.sleep(250);
        actions.release(dropElement);
        actions.perform();

        return this;
    }

    public DragAndDropPage dismissOrAcceptAlert(boolean dismissTrue) {
        String alertText;
        try {
            if (dismissTrue) {
                wait.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                alertText = alert.getText();
                log.info("Alert on page, text: " + alertText);
                alert.dismiss();
            } else {
                wait.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                alertText = alert.getText();
                log.info("Alert on page, text: " + alertText);
                alert.accept();
            }
        } catch (Exception e) {
            log.info("Alert isn't found");
        }
        return this;
    }

    public String getAletrText() {
        String alertText = null;
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alertText = alert.getText();
            log.info("Alert on page, text:" + alertText);
        } catch (Exception e) {
            log.info("Alert isn't found");
        }
        return alertText;
    }

    public boolean isElementOnPage(WebElement webElement) throws StaleElementReferenceException
    {
        try{
            wait.until(ExpectedConditions.visibilityOf(webElement));
            return true;
        } catch (StaleElementReferenceException e){
            log.error("Page isn't conteins this element: " + e);
            return false;
        }
    }

}


