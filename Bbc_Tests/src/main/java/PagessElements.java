import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by guska on 03.04.16.
 */
public enum PagessElements {

    NEWS_LINK(new ArrayList<String>(Arrays.asList("Watch/Listen"))),
    SPORT_LINK(new ArrayList<String>(Arrays.asList("Your essential sports diary"))),
    WEATHER_LINK(new ArrayList<String>(Arrays.asList(
            "Find a Forecast",
            "World Summary",
            //"Features & Analysis",
            "In Pictures",
            "About BBC Weather"))),
    SHOP_LINK(new ArrayList<String>(Arrays.asList("BBC Shop")));

    private ArrayList<String> pageTextElementsList;

    PagessElements (ArrayList<String> pageTextElementsList){
        this.pageTextElementsList = pageTextElementsList;
    }

    public ArrayList<String> getPageTextElementsList() {
        return pageTextElementsList;
    }
}
