import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;


/**
 * Created by guska on 15.03.16.
 */
public abstract class BasicPage{

    public static Logger log = Logger.getLogger(BasicPage.class);
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected String URL;
    protected ArrayList<String> pageTextElementsList;

    @FindBy(id = "idcta-username")
    protected WebElement ACC_USERNAME;

    public BasicPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    public boolean isElementOnPage(WebElement webElement)
    {
        try{
            wait.until(ExpectedConditions.visibilityOf(webElement));
            return true;
        }
        catch (ElementNotVisibleException e){
            log.error("Page isn't conteins this element: " + e);
            return false;
        }
    }

    public <T extends BasicPage> T openPage()
    {
        driver.get(URL);
        return (T) this;
    }

    public <T extends BasicPage> T clickElement(WebElement webElement)
    {
        try {
            wait.until(ExpectedConditions.visibilityOf(webElement)).click();
            return (T) this;
        }
        catch (ElementNotVisibleException e){
            log.error("Page isn't conteins this element: " + e);
            return null;
        }

    }

    public <T extends BasicPage> T clickElement(String xpath)
    {
        try {
            WebElement element = driver.findElement(By.xpath(xpath));
            wait.until(ExpectedConditions.visibilityOf(element)).click();
            return (T) this;
        }
        catch (ElementNotVisibleException e){
            log.error("Page isn't conteins this element: " + e);
            return null;
        }

    }

    public <T extends BasicPage> T typeInElement(WebElement webElement, String type)
    {
        try {
            wait.until(ExpectedConditions.visibilityOf(webElement));
            //webElement.click();
            webElement.clear();
            webElement.sendKeys(type);
            return (T) this;
        }
        catch (ElementNotVisibleException e)
        {
            log.error("Page isn't conteins this element: " + e);
            return null;
        }
    }

    public <T extends BasicPage> T checkAlert() {
        String alertText;
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alertText = alert.getText();
            log.info("Alert on page, text:" + alertText);
            alert.dismiss();
        } catch (Exception e) {
            log.info("checkAlert - Ok");
        }
        return (T) this;
    }

    public boolean isUserlogged() {
        if(!ACC_USERNAME.getText().contains("Sign in"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
