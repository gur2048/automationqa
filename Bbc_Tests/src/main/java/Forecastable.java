import org.openqa.selenium.WebElement;

/**
 * Created by guska on 28.03.16.
 */
public interface Forecastable {

    <T extends BasicPage> T openCityDistrict(String cityDistrict);
    <T extends BasicPage> T findCityDictriktInResult(String cityDistrict);
    String findCityForecastTitle();
}
