/**
 * Created by guska on 03.04.16.
 */
public enum HeaderLinks {


    NEWS_LINK("//div[@id='orb-nav-links']//a[contains(text(),'News')]", "http://www.bbc.com/news"),
    SPORT_LINK("//div[@id='orb-nav-links']//a[contains(text(),'Sport')]", "http://www.bbc.com/sport"),
    WEATHER_LINK("//div[@id='orb-nav-links']//a[contains(text(),'Weather')]", "http://www.bbc.com/weather/"),
    SHOP_LINK("//div[@id='orb-nav-links']//a[contains(text(),'Shop')]", "https://store.bbc.com/articles/bbc-shop-is-now-digital"),
    LOGIN_LINK("//span[@id='idcta-username']", "https://ssl.bbc.com/id/signin");

    private String xpath;
    private String url;

    HeaderLinks (String xpath, String url){
        this.xpath = xpath;
        this.url = url;
    }


    public String getUrl() {
        return url;
    }

    public String getXpath() {
        return xpath;
    }
}
