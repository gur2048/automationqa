import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by guska on 14.03.16.
 */
public class SportPage extends BasicPage{

        public SportPage(WebDriver driver) {
        super(driver);
        URL = HeaderLinks.SPORT_LINK.getUrl();
        pageTextElementsList = PagessElements.SPORT_LINK.getPageTextElementsList();
    }
}
