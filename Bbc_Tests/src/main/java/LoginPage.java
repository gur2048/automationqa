import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by guska on 28.03.16.
 */
public class LoginPage extends BasicPage implements Loginable{

    private String[] alerts= {
            LoginAlert.ALERT_STRING_TOO_SHORT.getXpath(),
            LoginAlert.ALERT_SPES_CHAR_CONTAINS.getXpath(),
            LoginAlert.ALERT_COMBINATION_NOT_RIGHT.getXpath(),
            LoginAlert.ALERT_THIS_NOT_QUITE_EMAIL.getXpath()
    };

    String SING_OUT_PAGE = "https://ssl.bbc.com/id/signout";

    @FindBy(xpath = "//div[@class='bbcid-field-container rememberme-container bbcid-valid bbcid-checkbox-container blq-clearfix enhanced']/a[@href='#']")
    private WebElement CHECKBOX_REMEMBER_LOG_PASS;

    @FindBy(xpath = "//p/span/a[@class='id4-cta-signout']")
    protected WebElement SING_OUT;

    @FindBy(id = "bbcid_submit_button")
    protected WebElement BUTTON_SINGIN;

    @FindBy(xpath = "//div[@class='bbcid-content']")
    protected WebElement BBC_ID_CONTENT;

    @FindBy(id = "bbcid-button cancel")
    protected WebElement BUTTON_CANCEL;

    @FindBy(id = "bbcid_unique")
    protected WebElement FIELD_LOGIN;

    @FindBy(id = "bbcid_password")
    protected WebElement FIELD_PASS;

    public LoginPage(WebDriver driver) {
        super(driver);
        URL = HeaderLinks.LOGIN_LINK.getUrl();
    }

    public LoginPage openPage()
    {
        driver.get(URL);
        return this;
    }

    public boolean isAllertMessageOnPage() {
        List<WebElement> webElementList;
        for (int i = 0; i < alerts.length; i++)
        {
            webElementList = (driver.findElements(By.xpath(alerts[i])));
            if (webElementList.size() != 0)
            {
                log.error("Incorrect login or pass. Page alert: "+ alerts[i]);
                return true;
            }
        }
            return  false;
    }

    public <T extends BasicPage> T rememberOnOff(boolean param) {

        boolean elementIsCheked = false;
        if (CHECKBOX_REMEMBER_LOG_PASS.getAttribute("class").equals("checked")) {
            elementIsCheked = true;
        }
        if (!param && elementIsCheked){
            CHECKBOX_REMEMBER_LOG_PASS.click();
        }

        return (T) this;
    }
}
