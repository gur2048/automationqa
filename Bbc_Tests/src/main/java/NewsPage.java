import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by guska on 14.03.16.
 */
public class NewsPage extends BasicPage{

    public NewsPage(WebDriver driver) {
        super(driver);
        URL = HeaderLinks.NEWS_LINK.getUrl();
        pageTextElementsList = PagessElements.NEWS_LINK.getPageTextElementsList();
    }
 }
