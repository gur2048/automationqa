import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by guska on 14.03.16.
 */
public class WeatherPage extends BasicPage implements Forecastable{

        private final String TITLE = "//*[@class='site-masthead']//*[@class='location-name']";
    private final String LOCATION_NAME_XPATH = "//*[@class='site-masthead']//*[@class='location-name']";

    @FindBy(xpath = "//div[@id='maps']")
    protected  WebElement MAP;

    @FindBy(xpath = "//div[@id='average-conditions']")
    protected  WebElement AVERAGE_CONDITIONS;

    @FindBy(xpath = "//div[@class='column-sidebar']//h2[contains(text(),'North America Forecast Video')]")
    protected  WebElement AMERICA_FORECAST_VIDEO;

    @FindBy(xpath = "//div[@class='column-sidebar']//h2[contains(text(),'Forecast Video')]")
    protected  WebElement FORECAST_VIDEO;

    @FindBy(xpath = "//div[@class='column-sidebar']//h2[contains(text(),'Europe Forecast Video')]")
    protected  WebElement EUROPE_FORECAST_VIDEO;

    @FindBy(xpath =  "//input[@id='locator-form-search']")
    protected  WebElement SEARCH_FILD;

    @FindBy(xpath = "//input[@id='locator-form-submit']")
    protected  WebElement SEARCH_BUTTON;

    public WeatherPage(WebDriver driver) {
        super(driver);
        URL = HeaderLinks.WEATHER_LINK.getUrl();
        pageTextElementsList = PagessElements.WEATHER_LINK.getPageTextElementsList();
    }

    public WeatherPage openCityDistrict(String cityDistrict)
    {
        List<WebElement> webElementList = new ArrayList<WebElement>();
        webElementList = driver.findElements(By.xpath(LOCATION_NAME_XPATH));
        if (webElementList.size() > 0)
        {
            log.error("This city has no district");
            return null;
        }
        try {
            clickElement(wait.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[@class='locator-results']//a[@title='" + cityDistrict + "']"))));
            return this;
        }
        catch (ElementNotVisibleException e){
            log.error("Page isn't conteins this element: " + e);
            return null;
        }
    }

    public WeatherPage findCityDictriktInResult(String cityDistrict)
    {
        try {
            wait.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[@class='locator-results']//a[@title='" + cityDistrict + "']")));
            return this;
        }
        catch (ElementNotVisibleException e){
            log.error("Page isn't conteins this element" + e);
            return null;
        }
    }

    public String findCityForecastTitle()
    {
        try {
            return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(TITLE)))).getText();
        }
        catch (ElementNotVisibleException e){
            log.error("Page isn't conteins this element" + e);
            return null;
        }
    }

}

