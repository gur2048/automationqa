import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

/**
 * Created by guska on 14.03.16.
 */
public class MainPage extends BasicPage{

    @FindBy(xpath = "//div[@id='orb-nav-links']//a[contains(text(),'Weather')]")
    protected  WebElement WEATHERLINK;

    String URL = "http://www.bbc.com/";

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public <T extends BasicPage> T openPage()
    {
        driver.get(URL);
        return (T) this;
    }

}
