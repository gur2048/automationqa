import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by guska on 14.03.16.
 */
public class ShopPage extends BasicPage{

    public ShopPage(WebDriver driver) {
        super(driver);
        URL = HeaderLinks.SHOP_LINK.getUrl();
        pageTextElementsList = PagessElements.SHOP_LINK.getPageTextElementsList();
    }
}
