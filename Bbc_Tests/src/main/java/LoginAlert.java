
/**
 * Created by guska on 05.04.16.
 */
public enum LoginAlert {

    ALERT_STRING_TOO_SHORT("//form[@id='bbcid-signin-form']//strong[contains(text(),'This is too short')]"),
    ALERT_SPES_CHAR_CONTAINS("//form[@id='bbcid-signin-form']//strong[contains(text(),'This contains special characters')]"),
    ALERT_COMBINATION_NOT_RIGHT("//form[@id='bbcid-signin-form']//strong[contains(text(),'This combination isn')]"),
    ALERT_THIS_NOT_QUITE_EMAIL("//form[@id='bbcid-signin-form']//strong[contains(text(),'quite an email address')]");

   private String xpath;

    LoginAlert (String xpath){
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }
}
