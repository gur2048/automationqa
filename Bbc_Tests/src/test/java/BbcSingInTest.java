import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by guska on 28.03.16.
 */
        /*
        1. open bbc.com
        2. check logged , if are logged in -  finish the test with log record
        3. open the sing_in page
        4. enter login and password to the fields
        5. check error message of login and the password input
        6. sing_in click
        7. check error message of login and the password input
        8. The message of the browser about saving the password to click cancel
        9. check redirect to the http://www.bbc.com/ page
        10. check user login
        11.redirect to the user page
        12. check text on page: 'You are signed in.'
        13. Sing out
        */

public class BbcSingInTest extends BbcAbstractTest{


    @BeforeTest
    public void beforeTest() {
        super.beforeTest();
        loginPage = new LoginPage(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    public void singInTest(){

        String login = "gur2048@gmail.com";
        String pass = "guska100";
        softAssert.assertFalse(loginPage.isUserlogged(), "User already sing in.");
        mainPage.clickElement(HeaderLinks.LOGIN_LINK.getXpath())
                .typeInElement(loginPage.FIELD_LOGIN, login)
                .typeInElement(loginPage.FIELD_PASS, pass);
        softAssert.assertFalse(loginPage.isAllertMessageOnPage(), "Incorrect login or pass");
        loginPage.rememberOnOff(false);
        loginPage.clickElement(loginPage.BUTTON_SINGIN)
                 .checkAlert();
        softAssert.assertFalse(loginPage.isAllertMessageOnPage(), "Incorrect login or pass");
        softAssert.assertEquals(driver.getCurrentUrl(), mainPage.URL, "Wrong page after sing in.");
        softAssert.assertTrue(mainPage.isUserlogged(), "User is not sing in.");
        mainPage.clickElement(HeaderLinks.LOGIN_LINK.getXpath());
        softAssert.assertTrue(loginPage.BBC_ID_CONTENT.getText()
                .contains("You are signed in."), "Wrong message on login page.");
        softAssert.assertTrue(loginPage.clickElement(loginPage.SING_OUT)
                .driver.getCurrentUrl()
                .contains(loginPage.SING_OUT_PAGE), "Wrong page");
        softAssert.assertAll();

        log.info("singInTest - passed");
    }

}
