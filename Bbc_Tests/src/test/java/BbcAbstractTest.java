import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

/**
 * Created by guska on 28.03.16.
 */
public abstract class BbcAbstractTest {
    static MainPage mainPage;
    static NewsPage newsPage;
    static SportPage sportPage;
    static ShopPage shopPage;
    static WeatherPage weatherPage;
    static LoginPage loginPage;
    static WebDriver driver;
    static Logger log;
    static SoftAssert softAssert;

    @BeforeTest
    public void beforeTest()
    {
        /*ProfilesIni profile = new ProfilesIni();
        FirefoxProfile testprofile = profile.getProfile("TestProf");*/
        driver = new FirefoxDriver();
        mainPage = new MainPage(driver);
        softAssert = new SoftAssert();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        log = Logger.getLogger(BbcHeaderTests.class);
    }

    @BeforeMethod
    public void beforeMethod()
    {
        mainPage.openPage();

    }

    @AfterTest
    public void afterTest()
    {
        driver.quit();
    }
}
