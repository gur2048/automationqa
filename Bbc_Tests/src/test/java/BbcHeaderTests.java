
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class BbcHeaderTests extends BbcAbstractTest{

    @BeforeTest
    public void beforeTest() {
        super.beforeTest();
        newsPage = new NewsPage(driver);
        sportPage = new SportPage(driver);
        shopPage = new ShopPage(driver);
        weatherPage = new WeatherPage(driver);
    }


    @DataProvider(name = "headerLinkTest")
    public static Object[][] headerLinkTest() {
        return new Object[][]{
                {HeaderLinks.NEWS_LINK, PagessElements.NEWS_LINK},
                {HeaderLinks.SPORT_LINK, PagessElements.SPORT_LINK},
                {HeaderLinks.SHOP_LINK, PagessElements.SHOP_LINK},
                {HeaderLinks.WEATHER_LINK, PagessElements.WEATHER_LINK}
        };
    }

    @Test(dataProvider = "headerLinkTest", priority = 0)
    public void headerLinkTest(HeaderLinks headerLinks, PagessElements pagessElements)
    {

        //check of the address of the current page
        Assert.assertEquals(mainPage.clickElement(headerLinks.getXpath())
                .driver.getCurrentUrl(), headerLinks.getUrl(), driver.getCurrentUrl()
                + " is wrong page, expected: "
                + headerLinks.getUrl());

        //verification of text markers on the page, markers can be more than one
           if(pagessElements.getPageTextElementsList().size() > 0)
        {
            String pageSrc = driver.getPageSource();
            for(int i = 0; i < pagessElements.getPageTextElementsList().size(); i++)
            {
                Assert.assertTrue(pageSrc.contains(pagessElements
                        .getPageTextElementsList()
                        .get(i)), "The text element is absent");
            }
        }
        else
        {
            log.error("pageTextElementsList is empty ");
        }
        log.info("HeaderLinkTest for " + headerLinks.getUrl() + " - passed");

    }
}
