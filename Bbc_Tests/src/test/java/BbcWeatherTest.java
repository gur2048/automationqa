
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;


public class BbcWeatherTest  extends BbcAbstractTest{

    @BeforeTest
    public void beforeTest() {
        super.beforeTest();
        weatherPage = new WeatherPage(driver);
    }

    @DataProvider(name = "bbcWeatherForecastTest")
    public static Object[][] bbcWeatherForecastTest() {
        return new Object[][]{
                {"New York", "New York, United States", "http://www.bbc.com/weather/5128581", weatherPage.AMERICA_FORECAST_VIDEO},
                {"London", "London, Greater London", "http://www.bbc.com/weather/2643743", weatherPage.FORECAST_VIDEO},
                {"Paris", "Paris Orly Airport, France", "http://www.bbc.com/weather/2988500", weatherPage.EUROPE_FORECAST_VIDEO},
                {"Budapest", "Budapest, Hungary", "http://www.bbc.com/weather/3054643", weatherPage.EUROPE_FORECAST_VIDEO}
        };
    }

    @Test(dataProvider = "bbcWeatherForecastTest")
    public void bbcWeatherForecastTest(String cityName, String districtName, String cityWeatherForecastUrl, WebElement forecastVideo/*, @Optional boolean param*/) {
        mainPage.clickElement(mainPage.WEATHERLINK);
        weatherPage.typeInElement(weatherPage.SEARCH_FILD, cityName)
                        .clickElement(weatherPage.SEARCH_BUTTON);
        softAssert.assertEquals(weatherPage.findCityDictriktInResult(districtName)
                        .openCityDistrict(districtName)
                        .driver.getCurrentUrl(), cityWeatherForecastUrl, "The address of the page is "
                + driver.getCurrentUrl()
                + " and isn't equal to expected - "
                + cityWeatherForecastUrl + ", for city "
                + cityName);

        softAssert.assertTrue(districtName.toUpperCase()
                  .contains(weatherPage.findCityForecastTitle()), weatherPage.findCityForecastTitle() + " - Title isn't equal to expected");
        softAssert.assertTrue(weatherPage.isElementOnPage(forecastVideo), "Forecast Video  isn't found on the page");
        softAssert.assertTrue(weatherPage.isElementOnPage(weatherPage.MAP), "Map isn't found on the page");
        softAssert.assertTrue(weatherPage.isElementOnPage(weatherPage.AVERAGE_CONDITIONS), "Average conditions isn't found on the page");
        softAssert.assertAll();

        log.info("Forecast test for " + cityName + ": " + districtName + " - passed");
    }

    @Test(priority = 2)
    @Parameters({"cityNameParam","districtNameParam","cityWeatherForecastUrlParam" })
    public void bbcWeatherForecastTest_byXML(String cityName, String districtName, String cityWeatherForecastUrl) {
        weatherPage = new WeatherPage(driver);

        mainPage.clickElement(mainPage.WEATHERLINK);
        weatherPage.typeInElement(weatherPage.SEARCH_FILD, cityName)
                   .clickElement(weatherPage.SEARCH_BUTTON);
        softAssert.assertEquals(weatherPage.findCityDictriktInResult(districtName)
                        .openCityDistrict(districtName)
                        .driver.getCurrentUrl(), cityWeatherForecastUrl, "The address of the page is "
                + driver.getCurrentUrl()
                + " and isn't equal to expected - "
                + cityWeatherForecastUrl
                + ", for city "
                + cityName);

        softAssert.assertTrue(districtName.toUpperCase()
                  .contains(weatherPage.findCityForecastTitle()), weatherPage.findCityForecastTitle() + " - Title isn't equal to expected");
        softAssert.assertTrue(weatherPage.isElementOnPage(weatherPage.FORECAST_VIDEO), "Forecast Video  isn't found on the page");
        softAssert.assertTrue(weatherPage.isElementOnPage(weatherPage.MAP), "Map isn't found on the page");
        softAssert.assertTrue(weatherPage.isElementOnPage(weatherPage.AVERAGE_CONDITIONS), "Average conditions isn't found on the page");
        softAssert.assertAll();

        log.info("Forecast test for " + cityName + " - passed");
    }
}


