import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by guska on 05.04.16.
 */
public class BbcLoginAlertTest extends  BbcAbstractTest{

    @BeforeTest
    public void beforeTest() {
        super.beforeTest();
        loginPage = new LoginPage(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @DataProvider(name = "loginAlertTest")
    public static Object[][] loginAlertTest() {
        return new Object[][]{
                {"a"      , "aaaaaaa", LoginAlert.ALERT_STRING_TOO_SHORT},
                {"aaaaaaa", "a"      , LoginAlert.ALERT_STRING_TOO_SHORT},
                {"bbbbbbb", "bbbbbbb", LoginAlert.ALERT_COMBINATION_NOT_RIGHT},
                {"{{{{{{{", "aaaaaaa", LoginAlert.ALERT_SPES_CHAR_CONTAINS},
                {"gur2048@", "aaaaaa", LoginAlert.ALERT_THIS_NOT_QUITE_EMAIL}
        };
    }

    @DataProvider(name = "successfulLoginTest")
    public static Object[][] successfulLoginTest() {
        return new Object[][]{
                {"gur2048@gmail.com", "guska100", HeaderLinks.LOGIN_LINK, mainPage.URL},
        };
    }

    @BeforeMethod
    public void beforeMethod()
    {
        loginPage.openPage();
    }

    @Test(dataProvider = "loginAlertTest", priority = 0)
    public void loginAlertTest(String login, String pass, LoginAlert alert){
        softAssert.assertTrue(loginPage.typeInElement(loginPage.FIELD_LOGIN, login)
                .typeInElement(loginPage.FIELD_PASS, pass)
                .clickElement(loginPage.BUTTON_SINGIN)
                .isElementOnPage(driver.findElement(By.xpath(alert.getXpath()))), "There is no alert on the page");

        softAssert.assertAll();

        log.info("singInTest - passed");
    }

    @Test(dataProvider = "successfulLoginTest", priority = 1)
    public void successfulLoginTest(String login, String pass, HeaderLinks links, String url){
        loginPage.rememberOnOff(false);// Remember me options, make turn off
        softAssert.assertTrue(loginPage.typeInElement(loginPage.FIELD_LOGIN, login)
                .typeInElement(loginPage.FIELD_PASS, pass)
                .clickElement(loginPage.BUTTON_SINGIN)
                .checkAlert()//Service alert check and alert.dismiss();
                .isElementOnPage(driver.findElement(By.xpath(links.getXpath()))), "There is no user name on the page. It isn't logged in");
        softAssert.assertEquals(driver.getCurrentUrl(), url, "Wrong page after login");
        softAssert.assertAll();

        log.info("successfulLoginTest - passed");
    }





}
