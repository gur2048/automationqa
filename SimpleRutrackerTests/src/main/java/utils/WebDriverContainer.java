package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;

public class WebDriverContainer {

	private WebDriver driver;


	public WebDriverContainer() {
		System.setProperty("webdriver.chrome.driver",
				new File(DirHolder.getDriversDir(), "chromedriver/chromedriver.exe").getAbsolutePath());
		System.setProperty("webdriver.ie.driver",
				new File(DirHolder.getDriversDir(), "iedriver/IEDriverServer.exe").getAbsolutePath());
	}

	public WebDriver getDriver() {
		if (driver == null) {
			driver = new FirefoxDriver(firefoxDriverProfile());
//			driver = new ChromeDriver();
//			driver = new InternetExplorerDriver();
			//driver.manage().window().setSize(new Dimension(1500, 1500));
			driver.manage().window().maximize();
		}
		return driver;
	}

	public void tearDown(){
		this.driver.quit();
	}

	protected FirefoxProfile firefoxDriverProfile() {
		File file = new File("src/main/resources/data/downloads/");
		String dirPath = file.getAbsoluteFile().getAbsolutePath();
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", dirPath);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/xlsx, application/msword");
		return profile;
	}



}
