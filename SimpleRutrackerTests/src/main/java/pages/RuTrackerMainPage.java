package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by guska on 15.04.16.
 */
public class RuTrackerMainPage extends AbstractPage {
    public RuTrackerMainPage(WebDriver driver) {
        super(driver);
    }

    //*[contains(text(), 'George Carlin')]

    public boolean sectionIsExistInParent (String parentSection, String childSection)
    {
        List<WebElement> listElement= driver.findElements(By.xpath("//a[.='"
                + childSection
                + "']/ancestor::div[@class='category']/h3[.='" + parentSection + "']"));
        if (listElement.size()>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int getAmountOfForums(int moreThan){

        List<WebElement> listElement= driver.findElements
                (By.xpath("(.//*[@class='forumline forum'])[1]//td[3][number(translate(.,',',''))>"
                        + moreThan
                        + "]"));
        return listElement.size();
    }

}
