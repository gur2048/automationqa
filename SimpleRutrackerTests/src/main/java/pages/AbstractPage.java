package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPage {

	public static final int DEFAULT_TIME_OUT_IN_SECONDS = 20;
	public static final String URL = "http://www.bbc.com/";

	protected final WebDriver driver;
	protected final WebDriverWait wait;

	public AbstractPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		PageFactory.initElements(driver, this);
	}

	public static String getURL() {
		return URL;
	}

	public void open() {
		driver.get(getURL());
	}



}
