import pages.RuTrackerMainPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RuTrackerTests extends  AbstractTest{

	@BeforeMethod
	public void beforeMethod() {
		super.beforeMethod();
		RuTrackerMainPage = new RuTrackerMainPage(driver);
	}

	@Test
	public void sectionIsExistInParentTest() {

		driver.get("http://rutracker.org/forum/index.php");
		softAssert.assertTrue(RuTrackerMainPage.sectionIsExistInParent("Кино, Видео и ТВ", "DVD Video"), "");
		softAssert.assertAll();
	}

	@Test
	public void AmountOfForumsTest() throws InterruptedException {

		driver.get("http://rutracker.org/forum/viewforum.php?f=124");
		Thread.sleep(2250);
		softAssert.assertEquals(RuTrackerMainPage.getAmountOfForums(1000), 3, "");
		softAssert.assertAll();
	}


}
