import pages.RuTrackerMainPage;
import utils.WebDriverContainer;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class AbstractTest {

	public static final int DEFAULT_TIME_OUT_IN_SECONDS = 20;

	protected WebDriverContainer container;

	protected static WebDriver driver;
	protected SoftAssert softAssert;
	protected RuTrackerMainPage RuTrackerMainPage;

	@BeforeSuite
	public void beforeSuite() {
		container = new WebDriverContainer();
		driver = container.getDriver();
		driver.manage().timeouts().implicitlyWait(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterSuite
	public void afterSuite() {
		driver.quit();
	}



}
